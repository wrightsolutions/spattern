#!/usr/bin/env python
#   Copyright 2017 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0
#

""" Tested initially using Python 2.7.5
Intended to have up to 2 arguments minimum so suffixed 2 

Choice is yours whether to have 'search' and 'match' interleaved in a single
specification file, or to use the full 3 arguments to specify separate file
for 'search' patterns and third argument to specify file for 'match' patterns

Because this program will [initially] fail silently if there are errors
in your patterns, you might want to define a simple match or two in that standalone
match file and have your more complex patterns in the standalone search file.
That way some successful extracts but some missing likely indicates a problem or two
in the file of 'search' patterns.
"""

from copy import deepcopy
import gc
import re
import select
from signal import signal, SIGHUP, SIGINT, SIGKILL, SIGTERM
import subprocess
from sys import argv,exit
from time import sleep

POLL_TIMEOUT_MILLIS = 3
SLEEP_SECS = 2

# ptailer is defined as a global so we can conveniently deal with signals
ptailer = None

def gc_triggered(signum, frame):
	#print("gc triggered by HUP.")
	collected = gc.collect()
	#print("Garbage collector: collected {0} objects.".format(collected))
	return


def sub_terminate(signum, frame):
	print("sub_terminate triggered by signum={0}".format(signum))
        sub_confirmed_dead = False
	if ptailer is None:
                return

        try:
		ptailer.terminate()
		ptailer.wait()
        except OSError as e:
                sub_confirmed_dead = True
	return


signal(SIGHUP, gc_triggered)
#signal(SIGINT, sub_terminate)
""" Above we prefer to use KeyboardException rather than catching SIGINT
Note that sigkill cannot be caught so we leave line below commented.
"""
#signal(SIGKILL, sub_terminate)
signal(SIGTERM, sub_terminate)

RE_BRACKETED_TIME_OR_NUM = re.compile(r'\[[\-T0-9.:]*\]')

matcher_list = []
searcher_list = []


def line_less_n_bracket_pairs(line,remove_pairs_max=2,stripper='both'):
	match_end_list = []
	for match in RE_BRACKETED_TIME_OR_NUM.finditer(line):
		match_end_list.append(match.end())

        try:
                if 0 == len(match_end_list):
                        unstripped = line
                elif remove_pairs_max > len(match_end_list):
                        unstripped = line[match_end_list[-1]:]
                else:
                        unstripped = line[match_end_list[remove_pairs_max-1]:]
        except IndexError as e:
                unstripped = line

        if 'both' == stripper:
                stripped = unstripped.strip()
        else:
                stripped = unstripped.lstrip()

	return stripped


def regexlines_to_dict(lines):
        """
        chr(44) is comma, chr(58) is colon (:)
        """
        matcher_appends = 0
        searcher_appends = 0
	for line in lines:
                try:
                        line_array = line.rstrip().split(chr(44))
                        pat_dict = {}
                        pat_dict['search_type'] = line_array[0]
                        pat_dict['action'] = line_array[1]
                        pat_dict['logname'] = line_array[2]
                        pat_dict['raw_or_string'] = line_array[3]
                        pat_dict['pattern'] = line_array[4]
                        # Next is a placeholder for the compiled re
                        pat_dict['rec'] = None
                except:
                        pat_dict = {}

                if len(pat_dict) < 5:
                        continue

                if 'match' == pat_dict['search_type']:
                        matcher_list.append(pat_dict)
                        matcher_appends+=1
                elif 'search' == pat_dict['search_type']:
                        searcher_list.append(pat_dict)
                        searcher_appends+=1
                else:
                        pass

        return (matcher_appends+searcher_appends)


def patternfile_to_memory(patfile_fullpath,list_to_print=None):
        """ Read in the regexes.
        Optionally print the supplied list on completion
        chr(47) is forward slash (/) ; chr(46) is dot / period
        """
        if patfile_fullpath is None:
                return 0
        elif len(patfile_fullpath) < 2:
                return 0
        else:
                if patfile_fullpath.count(chr(47)) < 2:
                        return 0

	with open(patfile_fullpath,'r') as rfile:
		to_dict_ret = regexlines_to_dict(rfile.readlines())
        if list_to_print is not None:
                print(list_to_print)
        return to_dict_ret


def list_of_dicts_compile(list_of_dicts):
        """ Returns an updated list_of_dicts where rec field
        should now contain a compiled regex.
        Where the list_of_dicts supplied does not seem right [too short]
        we simply return the original list_of_dicts unchanged.
        """
        if list_of_dicts is None or len(list_of_dicts) < 1:
                return list_of_dicts

        list_updated = deepcopy(list_of_dicts)

        for idx,re_dict in enumerate(list_of_dicts):
                pat = re_dict['pattern']
                raw_or_string = re_dict['raw_or_string']
                # Next save the compiled re in field 'rec'
                try:
                        # chr(34) double quote ; chr(39) single quote
                        if 'raw' == raw_or_string:
                                if 'erminate' in pat:
                                        print("compiling 'raw' pattern={0}".format(pat))
                                rec = re.compile(r"""%s""" % (pat))
                                # Above r"""
                        else:
                                # Below """
                                rec = re.compile("""%s""" % (pat))
                except Exception as exc:
                        rec = 0
                        rec_message = "{0} {1}".format(type(exc),str(exc))
                        re_dict['rec_message'] = rec_message
                        print(pat,rec_message)

                re_dict['rec'] = rec

                #if rec is not None and rec > 0:
                list_updated[idx] = re_dict

        return list_updated


def list_print_rec_plus(list_of_dicts,printer=True):
        """ For each dict in the list
        print rec field and some supplementary fields
        Set printer=False if you just want the list of reporting lines
        returned to you.
        """
        list_of_lines = []
        for idx,re_dict in enumerate(list_of_dicts):
                rec = re_dict['rec']
                pat = re_dict['pattern']
                line = "Pattern {0} was a ".format(idx)
                line = "{0}'{1}' attempt of".format(line,re_dict['search_type'])
                line = "{0} pattern ... {1} ... which ".format(line,pat)
                rec_result = 'is None'
                if rec is None:
                        pass
                elif 0 == rec:
                        rec_result = 'is ZERO so failed'
                        if 'rec_message' in re_dict:
                                recmess = re_dict['rec_message']
                                rec_result = "{0} with {1}".format(rec_result,recmess)
                else:
                        rec_result = 'succeeded :)'
                line = "{0}{1}".format(line,rec_result)
                list_of_lines = line
                if printer is not True:
                        continue
                print(line)

        return list_of_lines


def logfile_attach_loop(logfile_fullpath):

	global ptailer

        ptailer = subprocess.Popen(['tail','--follow=name','--retry',logfile_fullpath],\
                             stdout=subprocess.PIPE,stderr=subprocess.PIPE)
        p = select.poll()
        p.register(ptailer.stdout)

        PUSHES_TEXT = 'pushes extract to'

        while True:
                if p.poll(POLL_TIMEOUT_MILLIS):
                        ln = ptailer.stdout.readline()
                        for rmatch in matcher_list:                                
                                line_less = line_less_n_bracket_pairs(ln)
                                """
                                print("Checking mpat={0} for line={1}".format(rmatch['pattern'],
                                                                             line_less))
                                """
                                try:
                                        if rmatch['rec'].match(line_less):
                                                #print("match() success!")
                                                print("matched {0} {1}".format(PUSHES_TEXT,
                                                                               rmatch['logname']))
                                except AttributeError as e:
                                        print(rmatch['rec_message'])

                        for rsearch in searcher_list:
                                if rsearch['rec'].search(ln):
                                        print("searched {0} {1}".format(PUSHES_TEXT,
                                                                       rsearch['logname']))
                sleep(SLEEP_SECS)


if __name__ == '__main__':

	program_binary = argv[0].strip()

	log1 = None
        spat_fullpath = None
        mpat_fullpath = None
	if len(argv) > 1:
                log1 = argv[1].strip()
                if len(argv) > 2:
                        spat_fullpath = argv[2]
                        if len(argv) > 3:
                                mpat_fullpath = argv[3]

        #print(log1,spat_fullpath)
        if spat_fullpath is None:
                exit(121)

        """ Have got this far so know we have a minimum of 2 arguments
        and a fullpath defined for spat [ patterns to be used for search() ]
        """

        #appended = patternfile_to_memory(spat_fullpath,searcher_list)
        appended = patternfile_to_memory(spat_fullpath)
        #appended = patternfile_to_memory(mpat_fullpath,matcher_list)
        appended = patternfile_to_memory(mpat_fullpath)

        matcher_list_compiled = list_of_dicts_compile(matcher_list)
        #list_print_rec_plus(matcher_list_compiled,True)
        searcher_list_compiled = list_of_dicts_compile(searcher_list)
        #list_print_rec_plus(searcher_list_compiled,True)

	try:
	        logfile_attach_loop(log1)
	except KeyboardInterrupt:
		exit(102)


