# spattern tailextract programs

Follow the tail of a logfile extracting and acting on defined expressions


## pattern marked 'search' with action of 'insert into mariadb/mysql'

search,3306,,raw,assuming\s+zero\s+resource\s+counts


## pattern marked 'search' with action of log to 'sl_misconfigured.log'

search,0,sl_misconfigured,raw,smemory\s+limit\s+not\s+configured\s+for\s+this\s+node


## pattern marked 'search' with action of 'insert into mariadb/mysql' and log to 'sl_marker.log'

search,3306,sl_marker,raw,scheduler\s+plugin\s+loaded


## pattern marked 'search' with action of email hpcadmins@someorganisation.co.uk

search,hpcadmins,sl_error,raw,registration\s+node=cluster:\s+Invalid

Note: Non-numeric entry into second field indicates an email action


## pattern marked 'search' with action of 'insert into memcache' and log to 'sl_outofservice.log'

search,11211,sl_outofservice,string,to DRAIN

Note: The fourth field here is 'string' which means
 there will be no r prefix when compiling the regex


## Third field and standard prefix

Please consider using a prefix (Example sl_) when defining the third field to ensure avoid
 clashes with existing filenames such as the source log file


# spattern programs to process stdin incl. stdinextract.py


Process stdin text analysing all printable lines for patterns specified in regex files (usually suffixed .re)

