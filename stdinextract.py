#!/usr/bin/env python
#   Copyright 2017 Gary Wright http://www.wrightsolutions.co.uk/contact
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
#   On Debian systems, the complete text of the Apache license can be found in
#   /usr/share/common-licenses/Apache-2.0
#

""" Tested initially using Python 2.7.5
Intended to have up to 1 arguments minimum with a second optional argument

Both types match and search types of regex operation are supported.

Both types of regex specification are supported 'raw' and 'string' for each of above

Because this program will [initially] fail silently if there are errors
in your patterns, you might want to define a simple match or two in that standalone
match file and have your more complex patterns in the standalone search file.
That way some successful extracts but some missing likely indicates a problem or two
in the file of 'search' patterns.

Later variant stdinextractprintables.py ensures only textual output written

Example of run against c++ code using match and search patterns relevant to blockchain
cat src/governance.cpp | python ~/stdinextract.py inputregex/mextract_nheight6.re inputregex/sextract_nheight8.re
"""

#from collections import defaultdict
from copy import deepcopy
import fileinput
import gc
from os import getenv as osgetenv
from os import path as ospath
import re
import select
#from signal import signal, SIGHUP, SIGINT, SIGKILL, SIGTERM
from string import printable
from sys import argv,exit,stdin

DOTTY = chr(46)
RE_BRACKETED_TIME_OR_NUM = re.compile(r'\[[\-T0-9.:]*\]')

SET_PRINTABLE=set(printable)
TRANSLATE_DELETE = ''.join(map(chr, range(0,9)))
#print(len(TRANSLATE_DELETE))

PYVERBOSITY_STRING = osgetenv('PYVERBOSITY')
PYVERBOSITY = 1
if PYVERBOSITY_STRING is None:
	pass
else:
	try:
		PYVERBOSITY = int(PYVERBOSITY_STRING)
	except:
		pass
#print(PYVERBOSITY)

REGEX_COMPILE_ERROR_PREFIX = 'Regex compiling error for pattern'

matcher_list = []
mlist_of_fullpaths = []
searcher_list = []
slist_of_fullpaths = []

mpat_fullpath = None


def lines_joined(lines,joiner=chr(10)):
	""" chr(9) is tab  ; chr(10) is line feed ;
	chr(32) is space ; chr(61) is equals (=)
	"""
        joined = joiner.join(lines)
        return joined


def fullpathed(search_type,index1):
	""" 
	Final argument is called index1 as when fullpathed first being 1
	is easier to follow on filesystem for some.
	"""
	fullpathed = None
	if mpat_fullpath is None:
		return None
	type1 = None
	try:
		type1 = search_type[0]
	except:
		type1 = 'u'
	fullpath_to_append_to = mpat_fullpath
	if DOTTY == mpat_fullpath[0] and len(mpat_fullpath) > 1:
		fullpath_to_append_to = mpat_fullpath[1:]
	try:
		fullpathed = "{0}.{1}.{2}".format(fullpath_to_append_to,type1,index1)
	except:
		pass
	#ospath.join(blah,blah)
	return fullpathed


def write_to_fullpathed(search_type,idx,line_to_write):
	written_flag = False
	index1 = 1+idx
	fp_string = fullpathed(search_type,index1)
	if fp_string is None:
		return written_flag

	fp = None
	fp_open = None
	if 'match' == search_type:
		list_of_fullpaths = mlist_of_fullpaths
	else:
		list_of_fullpaths = slist_of_fullpaths
	fp = list_of_fullpaths[idx]

	if fp is None and len(fp_string.split(DOTTY)) > 2:
		try:
			fp_open = open(fp_string,'w')
			list_of_fullpaths[idx] = fp_open
		except:
			pass

	if fp is not None and fp_open is None:
		fp_open = fp

	# fp_open should either way reference a file open for writing
	try:
		fp_open.write(line_to_write)
		written_flag = True
	except:
		pass

	return written_flag


def close_all_fullpathed(list_of_fullpaths):
	closed_count = 0
	for fp in list_of_fullpaths:
		if fp is not None:
			try:
				fp.flush()
				fp.close()
				closed_count += 1
			except:
				pass
	return closed_count


def empty_if_unprintables(unfiltered):
	#stripped = unfiltered.strip()
	if set(unfiltered).issubset(SET_PRINTABLE):
		return unfiltered
	return ''


def line_less_n_bracket_pairs(line,remove_pairs_max=2,stripper='both'):
	match_end_list = []
	for match in RE_BRACKETED_TIME_OR_NUM.finditer(line):
		match_end_list.append(match.end())

        try:
                if 0 == len(match_end_list):
                        unstripped = line
                elif remove_pairs_max > len(match_end_list):
                        unstripped = line[match_end_list[-1]:]
                else:
                        unstripped = line[match_end_list[remove_pairs_max-1]:]
        except IndexError as e:
                unstripped = line

        if 'both' == stripper:
                stripped = unstripped.strip()
        else:
                stripped = unstripped.lstrip()

	return stripped


def regexlines_to_dict(search_type,lines):
        """
	For supplied search_type populate appropriate dict with regexes
	Lifted from previous work where search_types could be interleaved.
        """
        matcher_appends = 0
        searcher_appends = 0
	for line in lines:
                try:
                        pat_dict = {}
			raw_indicator_as_string = line[0]
			#print(raw_indicator_as_string)
			raw_or_string = int(raw_indicator_as_string)
			#print(raw_or_string)
                        pat_dict['raw_or_string'] = raw_or_string
                        pat_dict['pattern'] = line[1:].strip()
			#print(pat_dict['pattern'])
                        # Next is a placeholder for the compiled re
                        pat_dict['rec'] = None
                except:
                        pat_dict = {}

                if len(pat_dict) < 3:
                        continue

                if 'match' == search_type:
                        matcher_list.append(pat_dict)
                        matcher_appends+=1
                elif 'search' == search_type:
                        searcher_list.append(pat_dict)
                        searcher_appends+=1
                else:
                        pass

        return (matcher_appends+searcher_appends)


def patternfile_to_memory(search_type,patfile_fullpath,list_to_print=None):
        """ Read in the regexes.
        Optionally print the supplied list on completion
        chr(47) is forward slash (/) ; chr(46) is dot / period
        """
        if patfile_fullpath is None:
                return 0
        elif len(patfile_fullpath) < 2:
                return 0
        elif patfile_fullpath.endswith('.re'):
                pass
        else:
                if patfile_fullpath.count(chr(47)) < 2:
                        return 0

	with open(patfile_fullpath,'r') as rfile:
		if PYVERBOSITY is None or PYVERBOSITY > 1:
			print(search_type,patfile_fullpath)
		to_dict_ret = regexlines_to_dict(search_type,rfile.readlines())
        if list_to_print is not None:
                print(list_to_print)
        return to_dict_ret


def list_of_dicts_compile(list_of_dicts):
        """ Returns an updated list_of_dicts where rec field
        should now contain a compiled regex.
        Where the list_of_dicts supplied does not seem right [too short]
        we simply return the original list_of_dicts unchanged.
        """
        if list_of_dicts is None or len(list_of_dicts) < 1:
                return list_of_dicts

        list_updated = deepcopy(list_of_dicts)

        for idx,re_dict in enumerate(list_of_dicts):
                pat = re_dict['pattern']
                raw_or_string = re_dict['raw_or_string']
                # Next save the compiled re in field 'rec'
                try:
                        # chr(34) double quote ; chr(39) single quote
                        if 0 == raw_or_string:
				# Use r prefix when compiling this RAW type
                                if 'erminate' in pat and PYVERBOSITY > 1:
                                        print("compiling 'raw' pattern={0}".format(pat))
                                rec = re.compile(r"""%s""" % (pat))
                                # Above r"""
                        else:
                                # Indictation is this is typical STRING (may include spaces)
                                rec = re.compile("""%s""" % (pat))
                except Exception as exc:
                        rec = 0
                        rec_message = "{0} {1}".format(type(exc),str(exc))
                        re_dict['rec_message'] = rec_message
                        print(REGEX_COMPILE_ERROR_PREFIX,pat,rec_message)

                re_dict['rec'] = rec

                #if rec is not None and rec > 0:
                list_updated[idx] = re_dict

        return list_updated


def list_print_rec_plus(list_of_dicts,printer=True):
        """ For each dict in the list
        print rec field and some supplementary fields
        Set printer=False if you just want the list of reporting lines
        returned to you.
        """
        list_of_lines = []
        for idx,re_dict in enumerate(list_of_dicts):
                rec = re_dict['rec']
                pat = re_dict['pattern']
                line = "Pattern {0} was a ".format(idx)
                line = "{0} pattern ... {1} ... which ".format(line,pat)
                rec_result = 'is None'
                if rec is None:
                        pass
                elif 0 == rec:
                        rec_result = 'is ZERO so failed'
                        if 'rec_message' in re_dict:
                                recmess = re_dict['rec_message']
                                rec_result = "{0} with {1}".format(rec_result,recmess)
                else:
                        rec_result = 'succeeded :)'
                line = "{0}{1}".format(line,rec_result)
                list_of_lines = line
                if printer is not True:
                        continue
                print(line)

        return list_of_lines


def process_input(linelimit=0,verbosity=0):
	""" Process the input from stdin
	"""
	line_rc = 999

	idx = 1
        line = stdin.readline()

	while line:

		#print("processing line %d" % idx)
		line_rc = 0

		if not set(line).issubset(SET_PRINTABLE):
			pass
			#line_rc = 110
			#lines_with_problems += 1
			#print('line %s has one or more unprintables!' % idx)
			#continue


		for midx,rmatch in enumerate(matcher_list):
			line_less = line_less_n_bracket_pairs(line)
			"""
			print("Checking mpat={0} for line={1}".format(rmatch['pattern'],
										line_less))
			"""
			try:
                                #print(line_less)
				if rmatch['rec'].match(line_less):
					midx1=1+midx
					if PYVERBOSITY is None or PYVERBOSITY > 1:
						#print(midx)
						if len(rmatch['pattern']) >= 10:
							print("match() success on pattern={0}".format(rmatch['pattern']))
					write_to_fullpathed('match',midx,line)
			except AttributeError as e:
				print(rmatch['rec_message'])
				print('Quitting line processing early rc=1 because a match regex has a problem')
				return 1

		for sidx,rsearch in enumerate(searcher_list):
			try:
				if rsearch['rec'].search(line):
					sidx1=1+sidx
					#print(sidx1,sidx1,rsearch['pattern'])
					if PYVERBOSITY is None or PYVERBOSITY > 1:
						print("search() success on pattern={0}".format(rsearch['pattern']))
					write_to_fullpathed('search',sidx,line)
			except AttributeError as e:
				print(rsearch['rec_message'])
				print('Quitting line processing early rc=2 because a search regex has a problem')
				return 2

		"""						
		line_translated = line.translate(None,TRANSLATE_DELETE)
		if len(line) != len(line_translated):
			line_rc = 120
			lines_with_problems += 1
			print('line %s has tab or other low range chr()' % idx)
		"""

		if linelimit > 0:
			if idx >= linelimit:
				return 200

		line = stdin.readline()
		idx += 1

	return 0


def process_fileinput(finput):
        #lines_eb = finput.strip().split('\n')
	return process_lines(finput.strip().split('\n'))


if __name__ == '__main__':

	program_binary = argv[0].strip()

	log1 = None
        spat_fullpath = None
        mpat_fullpath = None
	if len(argv) > 1:
		# First argument is 'match' regexes
                mpat_fullpath = argv[1]
                if len(argv) > 2:
                        # Second argument is 'search' regexes
                        spat_fullpath = argv[2]

        #print(log1,mpat_fullpath)
        if mpat_fullpath is None:
                exit(121)

        """ Have got this far so know we have a minimum of 2 arguments
        and a fullpath defined for mpat [ patterns to be used for match() ]
        """

        #mappended = patternfile_to_memory(mpat_fullpath,matcher_list)
        mappended = patternfile_to_memory('match',mpat_fullpath)
	mlist_of_fullpaths = [None] * mappended
        #sappended = patternfile_to_memory(spat_fullpath,searcher_list)
        sappended = patternfile_to_memory('search',spat_fullpath)
	slist_of_fullpaths = [None] * sappended
	if PYVERBOSITY is None or PYVERBOSITY > 0:
		print(mappended,sappended)

	""" Having read in the regexes we next use re.compile to form
	precompiled regexes for use when iterating over the text input
	"""
        print(len(matcher_list),len(searcher_list))
        matcher_list_compiled = list_of_dicts_compile(matcher_list)
	if PYVERBOSITY is None or PYVERBOSITY > 0:
		if PYVERBOSITY > 1:
			list_print_rec_plus(matcher_list_compiled,True)
        searcher_list_compiled = list_of_dicts_compile(searcher_list)
	if PYVERBOSITY is None or PYVERBOSITY > 0:
		if PYVERBOSITY > 1:
			list_print_rec_plus(searcher_list_compiled,True)

	""" Next is where the iteration over text input is going to happen.
	We are using module fileinput to process what is sent into us via stdin
	"""
	#lret = process_lines(fileinput.input(inplace=False),0,1)
	lret = process_input()
	#lret = process_lines(fileinput.input(inplace=False),0,0)

	close_all_fullpathed(mlist_of_fullpaths)
	close_all_fullpathed(slist_of_fullpaths)

